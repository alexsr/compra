#!/usr/bin/env pythoh3

'''
Programa para mostrar la lista de la compra
'''

habitual = ('leche', 'pan', 'patatas')

def main():
    especifica = []
    print("Elemento a comprar: ")
    elemento = input()
    while elemento != "":
        especifica.append(elemento)
        print("Elemento a comprar: ")
        elemento = input()

    definitiva = []
    definitiva = list(especifica)
    for elemento in habitual:
        if elemento in definitiva:
            definitiva.remove(elemento)
    definitiva.extend(habitual)
    print("Lista de la compra:\n")
    for i in range(0, len(definitiva)):
        print(definitiva[i] + "\n")
    print("Elementos habituales: " , len(habitual))
    print("Elementos específicos: " , len(especifica))
    print("Elementos en lista: " , len(definitiva))





if __name__ == '__main__':
    main()